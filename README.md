# JupyterHub Spawner for SLURM with options form

This is a custom spawner for [Jupyterhub](https://jupyterhub.readthedocs.io/) that is designed for installations on clusters using the SLURM batch scheduling software. It includes an options form that is generated automatically based on the SLURM cluster configuration and current user associations.

This spawner extends the [batchspawner](https://github.com/jupyterhub/batchspawner) proposed by the JupyterHub community.

## Installation

```
pip install git+https://gitlab.com/ifb-elixirfr/cluster/utils/slurmspawner
```

## Usage

Here a sample jupyterhub_config.py section that you can use to enable the SLURM spawner :

```
batch_script = '''#!/bin/bash
#SBATCH --account={{account}}
#SBATCH --partition={{partition}}
#SBATCH --time={{runtime}}
#SBATCH --output={{homedir}}/.jupyter_%j.log
#SBATCH --job-name=jupyter
#SBATCH --chdir={{homedir}}
#SBATCH --cpus-per-task={{nprocs}}
#SBATCH --mem={{memory}}
#SBATCH --get-user-env=L
#SBATCH --export={{keepvars}}
{% if gres %}#SBATCH --gres={{gres}}{% endif %}
#SBATCH {{options}}
#SBATCH --nodes=1

which jupyterhub-singleuser
srun {{cmd}}
'''

c.JupyterHub.spawner_class = 'slurmspawner.FormSlurmSpawner'
c.SlurmSpawner.req_runtime = '12:00:00'
c.SlurmSpawner.batch_script = batch_script
c.Spawner.http_timeout = 120
```
